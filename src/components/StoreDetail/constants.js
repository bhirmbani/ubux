export default {
  GET_STORE_DETAIL: 'GET_STORE_DETAIL',
  GET_STORE_DETAIL_RESULT: 'GET_STORE_DETAIL_RESULT',
  GET_STORE_PRODUCT: 'GET_STORE_PRODUCT',
  GET_STORE_PRODUCT_RESULT: 'GET_STORE_PRODUCT_RESULT',
  GET_PRODUCT_DETAIL: 'GET_PRODUCT_DETAIL',
  GET_PRODUCT_DETAIL_RESULT: 'GET_PRODUCT_DETAIL_RESULT',
  PUSH_TO_SHOPPING_CART: 'PUSH_TO_SHOPPING_CART',
};
