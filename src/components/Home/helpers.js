

/**
 * This method will filter stores state by its status
 * @param {Object} state - state is initial state from home component
 * @param {Object} payload - payload object
 * @param {string} payload.type - type of the store that need to be filtered
 * either 'pending' or 'verified'
 * @param {string} payload.isSearch - to check if user user search input
 */
const filterStatusHelper = (state, payload) => {
  if (payload.isSearch) {
    return {
      ...state,
      filteredStores: {
        ...state.filteredStores,
        content: state.searchByTradingName.content.filter(each => each.status === payload.type),
        isFiltered: true,
      },
    };
  }
  return {
    ...state,
    filteredStores: {
      ...state.filteredStores,
      content: state.stores.filter(each => each.status === payload.type),
      isFiltered: true,
    },
  };
};

/**
 * This method will picked what data to render in CardList's Flatlist component
 */
const pickData = (props) => {
  if (props.filteredStores.isFiltered) {
    return { content: props.filteredStores.content };
  }
  if (props.searchByTradingName.isSearch) {
    return { content: props.searchByTradingName.content };
  }
  return { content: props.stores };
};

export default {
  filterStatusHelper,
  pickData,
};
