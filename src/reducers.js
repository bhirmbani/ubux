import { combineReducers } from 'redux';

import homeReducer from './components/Home/reducer';
import storeDetailReducer from './components/StoreDetail/reducer';
import shoppingCartReducer from './components/ShoppingCart/reducer';

/**
 * Combine all reducers each containers
 * Into one Javascript Object
 * @type {Maps}
 */
const rootReducer = combineReducers({
  home: homeReducer,
  storeDetail: storeDetailReducer,
  shoppingCart: shoppingCartReducer,
});

export default rootReducer;
