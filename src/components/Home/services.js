import lib from '../../lib';

export default {
  getAllStores: () => lib.api.get('/test/get-all-stores'),
  searchByTradingName: payload => lib.api.post('/test/search-store', payload),
};
