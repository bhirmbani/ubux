import React from 'react';
import { createStackNavigator } from 'react-navigation';

import ShoppingCartIcon from '../common';

// import another component here
import Components from '../components';

export default createStackNavigator({
  Home: {
    screen: Components.Home,
    navigationOptions: ({ navigation }) => ({
      headerRight: <ShoppingCartIcon navigation={navigation} />,
    }),
  },
  StoreDetail: {
    screen: Components.StoreDetail,
    navigationOptions: ({ navigation }) => ({
      headerRight: <ShoppingCartIcon navigation={navigation} />,
    }),
  },
  ProductDetail: {
    screen: Components.ProductDetail,
    navigationOptions: ({ navigation }) => ({
      headerRight: <ShoppingCartIcon navigation={navigation} />,
    }),
  },
  ShoppingCart: {
    screen: Components.ShoppingCart,
    navigationOptions: ({ navigation }) => ({
      headerRight: <ShoppingCartIcon navigation={navigation} />,
    }),
  },
}, {
  initialRouteName: 'Home',
  headerMode: 'screen',
  initialRouteKey: 'Home',
});
