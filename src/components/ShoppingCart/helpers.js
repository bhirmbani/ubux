
/**
 * This function remove the product from the array
 * @param {Object} state - reducer state of ShoppingCart/reducer
 * @param {number} idx index of product
 */
const removeCartHelper = (state, idx) => {
  state.shoppingCart.content.splice(idx, 1);
  return state.shoppingCart.content;
};

export default {
  removeCartHelper,
};
