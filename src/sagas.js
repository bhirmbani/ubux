import { all } from 'redux-saga/effects';

import homeSagas from './components/Home/sagas';
import storeDetailSagas from './components/StoreDetail/sagas';

export default function* rootSaga() {
  yield all([
    all(homeSagas),
    all(storeDetailSagas),
  ]);
}
