import axios from 'axios';
import config from '../config';

export const baseUrl = config.development.API_BASE_URL;

const instance = axios.create({
  baseURL: baseUrl,
});

export default instance;
