import React, { Component } from 'react';
import { SafeAreaView } from 'react-native';
import { Provider } from 'react-redux';
import { StyleProvider } from 'native-base';

import getTheme from './native-base-theme/components';
import platform from './native-base-theme/variables/platform';
import { store } from './src/store';


import RootView from './src';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <SafeAreaView style={{ flex: 1 }}>
          <StyleProvider style={getTheme(platform)}>
            <RootView />
          </StyleProvider>
        </SafeAreaView>
      </Provider>
    );
  }
}

export default App;
