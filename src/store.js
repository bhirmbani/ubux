import createSagaMiddleware from 'redux-saga';
import { createLogger } from 'redux-logger';
import { createStore, applyMiddleware, compose } from 'redux';

import rootReducer from './reducers';
import rootSaga from './sagas';

const sagaMiddleware = createSagaMiddleware();

const middleware = [
  process.env.NODE_ENV === 'development' && createLogger(),
  sagaMiddleware,
].filter(x => !!x);

export const store = createStore(rootReducer, compose(applyMiddleware(...middleware)));

export const test = () => 'test';

sagaMiddleware.run(rootSaga);
