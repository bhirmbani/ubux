import React from 'react';
import { View, FlatList } from 'react-native';
// import { OptimizedFlatList } from 'react-native-optimized-flatlist';
import PropTypes from 'prop-types';

import Card from './Card';
// import styles from './styles';
import helpers from './helpers';
import globalHelpers from '../../helpers';

// TODO: make CardList component reusable
const CardList = props => (
  <View>
    <FlatList
      columnWrapperStyle={globalHelpers.styles.columnWrapperStyle}
      ListFooterComponent={<View style={{ height: 0, marginBottom: 200 }} />}
      numColumns={2}
      data={helpers.pickData({
        stores: props.stores,
        filteredStores: props.filteredStores,
        searchByTradingName: props.searchByTradingName,
      }).content}
      keyExtractor={item => (props.searchByTradingName.isSearch ? item._id : item.storeId)}
      renderItem={({ item }) => <Card store={item} isSearch={props.searchByTradingName.isSearch} onPressStoreCard={props.onPressStoreCard} />}
    />
  </View>
);

CardList.propTypes = {
  stores: PropTypes.instanceOf(Array).isRequired,
  filteredStores: PropTypes.shape({
    isFiltered: PropTypes.bool,
    content: PropTypes.array,
  }),
  searchByTradingName: PropTypes.shape({
    isSearch: PropTypes.bool,
    content: PropTypes.array,
  }),
  onPressStoreCard: PropTypes.func.isRequired,
};

CardList.defaultProps = {
  // stores: [],
  filteredStores: {},
  searchByTradingName: {},
};

export default CardList;
