import { createSelector } from 'reselect';

/**
 * Subscribe to main reducer
 *
 * see './src/reducers.js'
 * @return Object
 */

export const selectHomeReducer = state => state.home;

export const selectAllStores = () => createSelector(selectHomeReducer, state => state.stores);

export const selectFilteredStores = () => createSelector(selectHomeReducer, state => state.filteredStores.content);

export const selectSearchStores = () => createSelector(selectHomeReducer, state => state.searchByTradingName.content);

export const selectIsFiltered = () => createSelector(selectHomeReducer, state => state.filteredStores.isFiltered);

export const selectIsSearch = () => createSelector(selectHomeReducer, state => state.searchByTradingName.isSearch);
