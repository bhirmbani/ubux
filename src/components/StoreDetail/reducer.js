import types from './constants';

// import helpers from './helpers';

const initialState = {
  storeDetail: {
    loading: false,
    message: '',
    content: {},
  },
  storeProduct: {
    loading: false,
    message: '',
    content: [],
  },
  productDetail: {
    loading: false,
    message: '',
    content: {},
  },
};

export default function storeDetailReducer(state = initialState, action) {
  switch (action.type) {
    case types.GET_STORE_DETAIL:
      return { ...state, storeDetail: { ...state.storeDetail, loading: true } };
    case types.GET_STORE_DETAIL_RESULT:
      return { ...state, storeDetail: { ...state.storeDetail, loading: false, message: action.payload.message, content: action.payload.content } };
    case types.GET_STORE_PRODUCT:
      return { ...state, storeProduct: { ...state.storeProduct, loading: true } };
    case types.GET_STORE_PRODUCT_RESULT:
      return { ...state, storeProduct: { ...state.storeProduct, loading: false, message: action.payload.message, content: action.payload.content } };
    case types.GET_PRODUCT_DETAIL:
      return { ...state, productDetail: { ...state.productDetail, loading: true } };
    case types.GET_PRODUCT_DETAIL_RESULT:
      return { ...state, productDetail: { ...state.productDetail, loading: false, message: action.payload.message, content: action.payload.content } };
    default:
      return state;
  }
}

/**
 * This action will get a store detail
 * Initiated by componentDidMount at StoreDetail component.
 * @param {string} payload - the store's id
 */
export const getStoreDetail = payload => ({
  type: types.GET_STORE_DETAIL,
  payload,
});

/**
 * This action will get result of getStoreDetail
 * @param {Object} payload - the payload object
 * @param {Object} payload.content - content of store detail
 * @param {string} payload.message - message to show
 */
export const getStoreDetailResult = payload => ({
  type: types.GET_STORE_DETAIL_RESULT,
  payload,
});

/**
 * This action will get store's product
 * Initiated by componentDidMount at StoreDetail component.
 * @param {string} payload - the store's id
 */
export const getStoreProduct = payload => ({
  type: types.GET_STORE_PRODUCT,
  payload,
});

/**
 * This action will get result of getStoreProduct
 * @param {Object} payload - the payload object
 * @param {Object} payload.content - content of store product
 * @param {string} payload.message - message to show
 */
export const getStoreProductResult = payload => ({
  type: types.GET_STORE_PRODUCT_RESULT,
  payload,
});

/**
 * This action will get a product detail
 * Initiated by componentDidMount at ProductDetail component.
 * @param {string} payload - the store's id
 */
export const getProductDetail = payload => ({
  type: types.GET_PRODUCT_DETAIL,
  payload,
});

/**
 * This action will get result of getProductDetail
 * @param {Object} payload - the payload object
 * @param {Object} payload.content - content of product detail
 * @param {string} payload.message - message to show
 */
export const getProductDetailResult = payload => ({
  type: types.GET_PRODUCT_DETAIL_RESULT,
  payload,
});

/**
 * This action push product to shopping cart
 * Reducer will be at ShoppingCart/reducer
 * @param { Object } payload - the payload object
 * @param { Object } payload.content - the product data
 */
export const pushToShoppingCart = payload => ({
  type: types.PUSH_TO_SHOPPING_CART,
  payload,
});
