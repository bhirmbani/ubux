import lib from '../../lib';

export default {
  getStoreDetail: payload => lib.api.get(`/test/get-store?storeId=${payload}`),
  getStoreProduct: payload => lib.api.get(`/test/get-store-products?storeId=${payload}`),
  getProductDetail: payload => lib.api.get(`/test/get-product?productId=${payload}`),
};
