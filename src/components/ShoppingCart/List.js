import React from 'react';
import { View, FlatList } from 'react-native';
import { H2 } from 'native-base';
import PropTypes from 'prop-types';

import styles from './styles';
import globalHelpers from '../../helpers';

import Item from './Item';

const ShoppingCartList = props => (
  <View>
    {props.shoppingCart.content.length > 0
      && (
        <FlatList
          ListHeaderComponent={(
            <View style={styles.ShoppingCartList.listHeaderParent}>
              <H2 style={styles.ShoppingCartList.listHeaderText}>My Cart</H2>
            </View>
        )}
          columnWrapperStyle={globalHelpers.styles.columnWrapperStyle}
          ListFooterComponent={<View style={{ height: 0, marginBottom: 40 }} />}
          numColumns={2}
          data={props.shoppingCart.content}
          keyExtractor={item => item._id}
          renderItem={({ item, index }) => (
            <Item shoppingCart={item} idx={index} onPressRemove={props.onPressRemove} />
          )}
        />)}
  </View>
);

ShoppingCartList.propTypes = {
  shoppingCart: PropTypes.shape({
    content: PropTypes.array,
  }).isRequired,
  onPressRemove: PropTypes.func.isRequired,
};

export default ShoppingCartList;
