import React from 'react';
import { View } from 'react-native';
import FastImage from 'react-native-fast-image';
import {
  Container, Content, Card, CardItem, Text, Button, Icon, Left, Body,
} from 'native-base';
import PropTypes from 'prop-types';

import styles from './styles';

const Front = props => (
  <View>
    <Card>
      <CardItem>
        <Left>
          <FastImage
            style={styles.Front.logoShape}
            source={{
              uri: 'https://via.placeholder.com/50',
              priority: FastImage.priority.low,
            }}
            resizeMode={FastImage.resizeMode.cover}
          />
          <Body>
            <Text>{props.store.content.tradingName ? props.store.content.tradingName : props.store.content.name}</Text>
            <Text note>{props.store.content.status}</Text>
          </Body>
        </Left>
      </CardItem>
      <CardItem>
        <Body>
          <FastImage
            style={styles.Front.imageShape}
            source={{
              uri: 'https://via.placeholder.com/400x200',
              priority: FastImage.priority.low,
            }}
            resizeMode={FastImage.resizeMode.cover}
          />
        </Body>
      </CardItem>
      <CardItem>
        <Left>
          <Button transparent textStyle={{ color: '#87838B' }}>
            <Icon type="FontAwesome" name="envelope-o" />
            <Text>{props.store.content.businessEmail ? props.store.content.businessEmail : 'No email'}</Text>
          </Button>
        </Left>
        <Left>
          <Button transparent textStyle={{ color: '#87838B' }}>
            <Icon type="FontAwesome" name="location-arrow" />
            <Text>{props.store.content.suburb}</Text>
          </Button>
        </Left>
      </CardItem>
    </Card>
  </View>
);

Front.propTypes = {
  store: PropTypes.shape({
    content: PropTypes.shape({
      tradingName: PropTypes.string,
      name: PropTypes.string,
      status: PropTypes.string,
      businessEmail: PropTypes.string,
      suburb: PropTypes.string,
    }),
  }).isRequired,
};

export default Front;
