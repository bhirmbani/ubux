import React from 'react';
import { View, Text } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { bindActionCreators } from 'redux';

// import styles from './styles';
import SearchInput from './SearchInput';
import FilterCheckBox from './FilterCheckBox';
import CardList from './CardList';

import * as actions from './reducer';
import * as selectors from './selectors';

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pending: false,
      verified: false,
      search: '',
    };
  }

  componentDidMount() {
    const { props } = this;
    props.getAllStores();
  }

  /**
   * This method will check and change status's state
   * @param { string } type - must be either 'pending' or 'verified'
   * @param { boolean } checked - state of the checkbox
   */
  onPressCheckBox = (type, checked) => {
    const { props } = this;
    this.setState({
      [type]: checked,
    });
    if (!checked) {
      return props.resetFilterStatus();
    }
    return props.filterStatus({ type, isSearch: props.isSearch });
  }

  onChangeSearchInput = (search) => {
    this.setState({
      search,
    });
  }

  onPressSubmitSearch = () => {
    const { props } = this;
    const { state } = this;
    const payload = {
      keyword: state.search,
    };
    props.searchByTradingName(payload);
  }

  onPressResetSearchInput = () => {
    const { props } = this;
    props.resetSearchInput();
    this.setState({
      search: '',
    });
  }

  /**
   * Navigate to StoreDetail
   * @param {string} id - store id
   */
  onPressStoreCard = (id) => {
    const { props } = this;
    const navigatePayload = {
      routeName: 'StoreDetail',
      params: { id },
      action: null,
      key: 'StoreDetail',
    };
    props.navigation.navigate(navigatePayload);
  }

  render() {
    const { pending, verified, search } = this.state;
    return (
      <View>
        <SearchInput
          onPressReset={this.onPressResetSearchInput}
          isSearch={this.props.isSearch}
          state={{ search }}
          onPressSubmitSearch={this.onPressSubmitSearch}
          onChangeSearchInput={this.onChangeSearchInput}
        />
        <FilterCheckBox onPressCheckBox={this.onPressCheckBox} state={{ pending, verified }} />
        <CardList
          onPressStoreCard={this.onPressStoreCard}
          stores={this.props.stores}
          filteredStores={{
            content: this.props.filteredStores,
            isFiltered: this.props.isFiltered,
          }}
          searchByTradingName={{
            content: this.props.searchStores,
            isSearch: this.props.isSearch,
          }}
        />
      </View>
    );
  }
}

Home.propTypes = {
  stores: PropTypes.instanceOf(Array),
  filteredStores: PropTypes.instanceOf(Array).isRequired,
  isFiltered: PropTypes.bool.isRequired,
  isSearch: PropTypes.bool.isRequired,
  searchStores: PropTypes.instanceOf(Array).isRequired,
};

Home.defaultProps = {
  stores: [],
};

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    getAllStores: actions.getAllStores,
    filterStatus: actions.filterStatus,
    searchByTradingName: actions.searchByTradingName,
    resetFilterStatus: actions.resetFilterStatus,
    resetSearchInput: actions.resetSearchInput,
  },
  dispatch,
);

const mapStateToProps = () => createStructuredSelector({
  stores: selectors.selectAllStores(),
  filteredStores: selectors.selectFilteredStores(),
  searchStores: selectors.selectSearchStores(),
  isFiltered: selectors.selectIsFiltered(),
  isSearch: selectors.selectIsSearch(),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
