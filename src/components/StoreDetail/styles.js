import { StyleSheet } from 'react-native';

import helpers from '../../helpers';

const Front = StyleSheet.create({
  imageShape: {
    width: helpers.styles.FULL_WIDTH - 50,
    height: helpers.styles.FULL_HEIGHT / 3,
  },
  logoShape: {
    width: 50,
    height: 50,
  },
});

const Product = StyleSheet.create({
  parentShape: {
    margin: 10,
  },
  childrenShape: {
    textAlign: 'center',
    width: helpers.styles.WIDTH_HALF - 20,
    height: helpers.styles.FULL_HEIGHT / 5 + 15,
    alignItems: 'center',
    // borderWidth: 1,
    // borderRadius: 10,
    // borderColor: helpers.styles.constantStyles.borderColor,
  },
  childrenImage: {
    width: helpers.styles.WIDTH_HALF - 20,
    height: helpers.styles.FULL_HEIGHT / 5 - 35,
    borderRadius: 10,
  },
  textParent: {
    marginVertical: 5,
  },
});

const StoreProducts = StyleSheet.create({
  listHeaderParent: {
    margin: 10,
  },
  listHeaderText: {
    textAlign: 'center',
  },
  childrenImage: {
    width: helpers.styles.WIDTH_HALF - 20,
    height: helpers.styles.FULL_HEIGHT / 5 - 35,
    borderRadius: 10,
  },
});

const ProductDetail = StyleSheet.create({
  parentShape: {
    margin: 10,
  },
  childrenShape: {
    textAlign: 'center',
    width: helpers.styles.FULL_WIDTH - 20,
    // height: helpers.styles.FULL_HEIGHT / 5 + 15,
    alignItems: 'center',
    // borderWidth: 1,
    borderRadius: 10,
    // borderColor: helpers.styles.constantStyles.borderColor,
  },
  childrenImage: {
    width: helpers.styles.FULL_WIDTH - 20,
    height: helpers.styles.FULL_HEIGHT / 3 - 35,
    borderRadius: 10,
  },
  listHeaderParent: {
    margin: 10,
  },
  listHeaderText: {
    textAlign: 'center',
  },
  textParent: {
    justifyContent: 'space-between',
    width: helpers.styles.FULL_WIDTH,
    display: 'flex',
    flexDirection: 'row',
    marginVertical: 10,
  },
  textChild: {
    justifyContent: 'center',
    width: helpers.styles.WIDTH_HALF - 30,
    marginHorizontal: 10,
  },
});

export default {
  Front,
  Product,
  StoreProducts,
  ProductDetail,
};
