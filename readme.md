# Docs

### How to install

#### Global
1. `react-native link react-native-fast-image`
2. `react-native link react-native-vector-icons`

#### IOS
1. `npm install -g react-native-cli`
2. follow this: [Getting Started · React Native](https://facebook.github.io/react-native/docs/getting-started#xcode) until command line tools
3. `yarn` or `npm install`
4. on the root directory: `npm start`
5. still at the same root directory on the new terminal tab:  `npm run ios`
6. follow this: https://github.com/facebook/react-native/issues/21741#issuecomment-429193933
7. The build should succeed

#### Android
1. Make sure android emulator running
2. on the root directory `npm start`
3. new tab in the same root directory `npm run android`
4. the build should work

#### Testing
`npm run test` or `yarn test`

### Folder structure
src
this is where component and other supporting file located

#### src/index.js
This file will host main navigation file (for now).

#### src/lib/index.js
This file will host library helper

#### src/config/index.js
This file will host config file

#### src/helpers/index.js
This file will consist helper function for most common operation that reusable in most component

#### src/navigation/index.js
main file for navigation

#### src/common/index.js
main file for reusable component

There will be a stack navigator with following object:
* Home
This is the top of the app. The first screen user saw (for now).

### Fixed folder structure
#### All of src/components/<Folder_name> will have these files:
1. index.js
main entrance for other components
2. reducer.js
this file contain action and reducer
3. sagas.js
file to contain sagas call
4. constants.js
file to contain constants for action type
5. styles.js
file to contain styles object
6. services.js
File to keep endpoint for each call to the backend
7. helpers.js
helper file that consist function that needed by this component and its child

#### src/components/index.js
main entrance for other components.

#### src/components/Home/styles.js
styles object for each components in Home folder
any styles object will be have property name with capital on first letter. It’s indicated the owner of the style.

#### src/components/Home/index.js
main file for Home component.

#### src/components/Home/Card.js
this is card component that consist general info from each store.

#### src/components/Home/CardList.js
this component will host the Card component and show list of the store to the user.

#### src/components/Home/SearchInput.js
This component will receive user input to search store based on its tradingName

#### src/components/Home/FilterCheckBox.js
This component is the checkbox that will filter store based on its status

#### src/store.js
This is main redux store file

#### src/sagas.js
This is main file for sagas

#### src/reducers.js
A file to combine all of the reducers