import { put, call, takeLatest } from 'redux-saga/effects';
// import Reactotron from 'reactotron-react-native';
import Api from './services';
import types from './constants';
import * as actions from './reducer';
// import * as selectors from './selectors'

/**
 * This method will get all stores from database.
 */
function* watchGetAllStores() {
  try {
    const response = yield call(Api.getAllStores);
    const { data } = response;
    const resultPayload = {
      stores: data.stores,
      message: data.message,
    };
    if (data.success) {
      return yield put(actions.getAllStoresResult(resultPayload));
    }
    return yield put(actions.getAllStoresResult({ ...resultPayload, stores: [] }));
  } catch (error) {
    return yield put(actions.getAllStoresResult({ stores: [], message: error.message }));
  }
}

/**
 * This method will search store based on its tradingName
 * @param {Object} payload - The payload object
 * @param {Object} payload.keyword - keyword of the tradingName to be search
 */
function* watchSearchStoreByTradingName({ payload }) {
  try {
    const response = yield call(Api.searchByTradingName, payload);
    const { data } = response;
    const resultPayload = {
      content: data.stores,
      message: data.message,
    };
    if (data.success) {
      return yield put(actions.searchByTradingNameResult(resultPayload));
    }
    return yield put(actions.getAllStoresResult({ ...resultPayload, content: [] }));
  } catch (error) {
    return yield put(actions.getAllStoresResult({ content: [], message: error.message }));
  }
}

const homeSagas = [
  takeLatest(types.GET_ALL_STORES, watchGetAllStores),
  takeLatest(types.SEARCH_BY_TRADING_NAME, watchSearchStoreByTradingName),
];

export default homeSagas;
