import React from 'react';
import renderer from 'react-test-renderer';

import FilterCheckBox from '../src/components/Home/FilterCheckBox';


it('renders a FilterCheckBox both state false', () => {
  expect(renderer.create(
    <FilterCheckBox
      onPressCheckBox={jest.fn()}
      state={{
        pending: false,
        verified: false,
      }}
    />,
  )).toMatchSnapshot();
});

it('renders a FilterCheckBox pending true', () => {
  expect(renderer.create(
    <FilterCheckBox
      onPressCheckBox={jest.fn()}
      state={{
        pending: true,
        verified: false,
      }}
    />,
  )).toMatchSnapshot();
});

it('renders a FilterCheckBox verified true', () => {
  expect(renderer.create(
    <FilterCheckBox
      onPressCheckBox={jest.fn()}
      state={{
        pending: false,
        verified: true,
      }}
    />,
  )).toMatchSnapshot();
});
