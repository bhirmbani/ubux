import { Dimensions, StyleSheet } from 'react-native';

export const window = Dimensions.get('window');

export const FULL_WIDTH = window.width;
export const FULL_HEIGHT = window.height;

export const IMAGE_HEIGHT = window.height / 4;
export const IMAGE_HEIGHT_SMALL = window.height / 8;
export const IMAGE_WIDTH_SMALL = window.width / 8;
export const WIDTH_HALF = window.width / 2;

export const constantStyles = {
  borderColor: '#f7f7f7',
};

export const columnWrapperStyle = {
  flex: 1,
  justifyContent: 'space-between',
};
