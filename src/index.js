import React from 'react';

import AppNavigator from './navigation';


class RootView extends React.Component {
  componentDidMount() {
  }

  render() {
    return (
      <AppNavigator />
    );
  }
}

export default RootView;
