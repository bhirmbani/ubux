import Home from './Home';
import StoreDetail from './StoreDetail';
import ProductDetail from './StoreDetail/ProductDetail';
import ShoppingCart from './ShoppingCart';

export default {
  Home,
  StoreDetail,
  ProductDetail,
  ShoppingCart,
};
