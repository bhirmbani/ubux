import types from './constants';

import helpers from './helpers';

const initialState = {
  getStores: {
    loading: false,
    message: '',
  },
  stores: [],
  filteredStores: {
    content: [],
    isFiltered: false,
  },
  searchByTradingName: {
    loading: false,
    message: '',
    content: [],
    isSearch: false,
  },
};

export default function homeReducer(state = initialState, action) {
  switch (action.type) {
    case types.GET_ALL_STORES:
      return { ...state, getStores: { ...state.getStores, loading: true } };
    case types.GET_ALL_STORES_RESULT:
      return { ...state, getStores: { ...state.getStores, loading: false, message: action.payload.message }, stores: action.payload.stores };
    case types.FILTER_STATUS:
      return helpers.filterStatusHelper(state, { type: action.payload.type, isSearch: action.payload.isSearch });
    case types.SEARCH_BY_TRADING_NAME:
      return { ...state, searchByTradingName: { ...state.searchByTradingName, loading: true } };
    case types.SEARCH_BY_TRADING_NAME_RESULT:
      return { ...state, searchByTradingName: { ...state.searchByTradingName, message: action.payload.message, content: action.payload.content, loading: false, isSearch: true } };
    case types.RESET_FILTER_STATUS:
      return { ...state, filteredStores: { ...state.filteredStores, isFiltered: false } };
    case types.RESET_SEARCH_INPUT:
      return { ...state, searchByTradingName: { ...state.searchByTradingName, isSearch: false } };
    default:
      return state;
  }
}

/**
 * This action will init getAllStores sagas call to the backend.
 * Initiated by componentDidMount at Home component.
 */
export const getAllStores = () => ({
  type: types.GET_ALL_STORES,
});

/**
 * This action receive the result of getAllStores action
 * The result will be either success or false (error happened in client side).
 * @param {Object} payload - the payload object
 * @param {string} payload.message - message that need to be displayed in client
 * @param {Array} payload.stores - stores data
 */
export const getAllStoresResult = payload => ({
  type: types.GET_ALL_STORES_RESULT,
  payload,
});

/**
 * This action will initiated filter store by its status
 * @param {Object} payload - the payload object
 * @param {string} payload.type - the type of the status to be filtered.
 * either isPending or isVerified
 * @param {boolean} payload.isSearch - payload to check if user use searchinput
 */
export const filterStatus = payload => ({
  type: types.FILTER_STATUS,
  payload,
});

/**
 * This action will init search store based on its tradingName
 * @param {Object} payload - the payload object
 * @param {string} payload.keyword - minimum 3 character
 */
export const searchByTradingName = payload => ({
  type: types.SEARCH_BY_TRADING_NAME,
  payload,
});

/**
 * This action receive the result of searchByTradingNameAction
 * @param {Object} payload - the payload object
 * @param {string} payload.message - the message for the user
 * @param {array} payload.content - the result of the search
 */
export const searchByTradingNameResult = payload => ({
  type: types.SEARCH_BY_TRADING_NAME_RESULT,
  payload,
});

/**
 * This action reset store's filter
 */
export const resetFilterStatus = () => ({
  type: types.RESET_FILTER_STATUS,
});

/**
 * This action reset search input
 */
export const resetSearchInput = () => ({
  type: types.RESET_SEARCH_INPUT,
});
