import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
// import { bindActionCreators } from 'redux';

// import * as actions from './reducer';
import * as selectors from '../components/ShoppingCart/selectors';

class ShoppingCartIcon extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onPressIcon = () => {
    const navigatePayload = {
      routeName: 'ShoppingCart',
      params: {},
      action: null,
      key: 'ShoppingCart',
    };
    this.props.navigation.navigate(navigatePayload);
  }

  render() {
    return (
      <TouchableOpacity
        onPress={this.onPressIcon}
        style={{
          width: 60, height: 50, justifyContent: 'center', position: 'relative',
        }}
      >
        {this.props.shoppingCart.content.length > 0
       && (
       <View style={{
         position: 'absolute',
         backgroundColor: 'red',
         width: 20,
         borderRadius: 10,
         zIndex: 100,
         justifyContent: 'center',
         right: 25,
         top: 5,
       }}
       >
         <Text style={{ color: 'white', textAlign: 'center' }}>
           {this.props.shoppingCart.count}
         </Text>
       </View>
       )}
        <Icon style={{ width: 30, height: 30 }} type="FontAwesome" name="shopping-cart" />
      </TouchableOpacity>
    );
  }
}

// const mapDispatchToProps = dispatch => bindActionCreators(
//   {
//     getAllStores: actions.getAllStores,
//     filterStatus: actions.filterStatus,
//     searchByTradingName: actions.searchByTradingName,
//     resetFilterStatus: actions.resetFilterStatus,
//     resetSearchInput: actions.resetSearchInput,
//   },
//   dispatch,
// );

const mapStateToProps = () => createStructuredSelector({
  shoppingCart: selectors.selectShoppingCart(),
});

export default connect(mapStateToProps, null)(ShoppingCartIcon);
