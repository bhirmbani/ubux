import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { H3 } from 'native-base';
import PropTypes from 'prop-types';
import FastImage from 'react-native-fast-image';

import styles from './styles';

const Product = props => (
  <TouchableOpacity
    style={styles.Product.parentShape}
    key={props.product._id}
    onPress={() => props.onPressProduct(props.product._id)}
  >
    <View style={styles.Product.childrenShape}>
      <FastImage
        style={styles.Product.childrenImage}
        source={{
          uri: 'https://via.placeholder.com/150',
          priority: FastImage.priority.low,
        }}
        resizeMode={FastImage.resizeMode.cover}
      />
      <View style={styles.Product.textParent}>
        <H3>{props.product.name}</H3>
      </View>
    </View>
  </TouchableOpacity>
);

Product.propTypes = {
  product: PropTypes.shape({
    _id: PropTypes.string,
    name: PropTypes.string,
  }).isRequired,
  onPressProduct: PropTypes.func.isRequired,
};

export default Product;
