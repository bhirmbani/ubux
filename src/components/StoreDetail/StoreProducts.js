import React from 'react';
import { View, FlatList } from 'react-native';
import { H2 } from 'native-base';
import PropTypes from 'prop-types';

import styles from './styles';
import globalHelpers from '../../helpers';

import Product from './Product';

const StoreProducts = props => (
  <View>
    {props.products.content.length > 0
      ? (
        <FlatList
          ListHeaderComponent={(
            <View style={styles.StoreProducts.listHeaderParent}>
              <H2 style={styles.StoreProducts.listHeaderText}>Store&apos;s Product</H2>
            </View>
        )}
          columnWrapperStyle={globalHelpers.styles.columnWrapperStyle}
          ListFooterComponent={<View style={{ height: 0, marginBottom: 40 }} />}
          numColumns={2}
          data={props.products.content}
          keyExtractor={item => item._id}
          renderItem={({ item }) => (
            <Product product={item} onPressProduct={props.onPressProduct} />
          )}
        />
      ) : (
        <View style={styles.StoreProducts.listHeaderParent}>
          <H2 style={styles.StoreProducts.listHeaderText}>
          This store doesn&apos;t have any product
          </H2>
        </View>
      )}
  </View>
);

StoreProducts.propTypes = {
  products: PropTypes.shape({
    content: PropTypes.array,
  }).isRequired,
  onPressProduct: PropTypes.func.isRequired,
};

export default StoreProducts;
