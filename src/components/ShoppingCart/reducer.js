import types from './constants';

import helpers from './helpers';

const initialState = {
  shoppingCart: {
    count: 0,
    content: [],
  },
};

export default function shoppingCartReducer(state = initialState, action) {
  switch (action.type) {
    case types.PUSH_TO_SHOPPING_CART:
      return {
        ...state,
        shoppingCart: {
          ...state.shoppingCart,
          count: state.shoppingCart.count + 1,
          content: [...state.shoppingCart.content, action.payload.content],
        },
      };
    case types.REMOVE_FROM_CART:
      return {
        ...state,
        shoppingCart: {
          ...state.shoppingCart,
          count: state.shoppingCart.count - 1,
          content: helpers.removeCartHelper(state, action.payload.idx),
        },
      };
    default:
      return state;
  }
}

/**
 * This action remove a product from shopping cart
 * @param {number} payload - idx of the product
 */
export const removeFromCart = payload => ({
  type: types.REMOVE_FROM_CART,
  payload,
});
