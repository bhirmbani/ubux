import React from 'react';
import { View, ScrollView } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { bindActionCreators } from 'redux';

// import styles from './styles';
import Front from './Front';
import StoreProducts from './StoreProducts';
import ProductDetail from './ProductDetail';

import * as actions from './reducer';
import * as selectors from './selectors';

class StoreDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    const { props } = this;
    props.getStoreDetail(props.navigation.state.params.id);
    props.getStoreProduct(props.navigation.state.params.id);
  }

  componentDidUpdate(prevProps) {
    const { props } = this;
    if (this.props.productDetail.content !== prevProps.productDetail.content) {
      const navigatePayload = {
        routeName: 'ProductDetail',
        params: {
          productDetail: this.props.productDetail,
          pushToShoppingCart: this.onPressPushToShoppingCart,
        },
        action: null,
        key: 'ProductDetail',
      };
      props.navigation.navigate(navigatePayload);
    }
  }

  /**
   * This method will push product data to shopping cart
   * and iterate the counter
   * @param {Object} product - the product data
   */
  onPressPushToShoppingCart = (product) => {
    const payload = {
      content: product,
    };
    this.props.pushToShoppingCart(payload);
  }

  /**
   * Navigate to ProductDetail and pass product id as params
   * @param {string} id - product's id
   */
  onPressProduct = (id) => {
    const { props } = this;
    props.getProductDetail(id);
  }

  render() {
    return (
      <ScrollView>
        <Front store={this.props.store} />
        <StoreProducts
          products={this.props.storeProduct}
          onPressProduct={this.onPressProduct}
          // productDetail={this.props.productDetail}
        />
      </ScrollView>
    );
  }
}

StoreDetail.propTypes = {
  store: PropTypes.shape({
    loading: PropTypes.bool,
    message: PropTypes.string,
    content: PropTypes.object,
  }).isRequired,
  storeProduct: PropTypes.shape({
    loading: PropTypes.bool,
    message: PropTypes.string,
    content: PropTypes.array,
  }).isRequired,
  productDetail: PropTypes.shape({
    loading: PropTypes.bool,
    message: PropTypes.string,
    content: PropTypes.object,
  }).isRequired,
  pushToShoppingCart: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    getStoreDetail: actions.getStoreDetail,
    getStoreProduct: actions.getStoreProduct,
    getProductDetail: actions.getProductDetail,
    pushToShoppingCart: actions.pushToShoppingCart,
  },
  dispatch,
);

const mapStateToProps = () => createStructuredSelector({
  store: selectors.selectStoreDetail(),
  storeProduct: selectors.selectStoreProduct(),
  productDetail: selectors.selectProductDetail(),
});

export default connect(mapStateToProps, mapDispatchToProps)(StoreDetail);
