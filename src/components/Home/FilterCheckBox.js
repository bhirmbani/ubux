import React from 'react';
import { View, Text } from 'react-native';
import { CheckBox, ListItem, Body } from 'native-base';
import PropTypes from 'prop-types';

import styles from './styles';

const CheckBoxComponent = props => (
  <View style={styles.FilterChecklist.parentShape}>
    <ListItem style={styles.FilterChecklist.childrenShape}>
      <CheckBox disabled={props.state.verified} onPress={() => props.onPressCheckBox('pending', !props.state.pending)} checked={props.state.pending} color="red" />
      <Body>
        <Text style={styles.FilterChecklist.textShape}>Pending</Text>
      </Body>
    </ListItem>
    <ListItem style={styles.FilterChecklist.childrenShape}>
      <CheckBox disabled={props.state.pending} onPress={() => props.onPressCheckBox('verified', !props.state.verified)} checked={props.state.verified} color="green" />
      <Body>
        <Text style={styles.FilterChecklist.textShape}>Verified</Text>
      </Body>
    </ListItem>
  </View>
);

CheckBoxComponent.propTypes = {
  onPressCheckBox: PropTypes.func.isRequired,
  state: PropTypes.shape({
    verified: PropTypes.bool.isRequired,
    pending: PropTypes.bool.isRequired,
  }).isRequired,
};

export default CheckBoxComponent;
