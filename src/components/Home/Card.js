import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import FastImage from 'react-native-fast-image';

import styles from './styles';

const Card = props => (
  <TouchableOpacity
    onPress={() => props.onPressStoreCard(props.isSearch ? props.store._id : props.store.storeId)}
    style={styles.Card.parentShape}
    key={props.isSearch ? props.store._id : props.store.storeId}
  >
    <View style={styles.Card.childrenShape}>
      <FastImage
        style={styles.Card.childrenImage}
        source={{
          uri: 'https://via.placeholder.com/150',
          priority: FastImage.priority.low,
        }}
        resizeMode={FastImage.resizeMode.cover}
      />
      <View style={styles.Card.textParent}>
        {props.store.storeId
          && (
          <Text style={{ fontSize: 8 }}>
            Store id:
            {' '}
            {props.store.storeId}
          </Text>
          )}
        {props.store.tradingName
          && (
          <Text style={{ fontSize: 8 }}>
            Trading name:
            {' '}
            {props.store.tradingName}
          </Text>
          )}
        <Text>
        status:
          {' '}
          {props.store.status}
        </Text>
      </View>
    </View>
  </TouchableOpacity>
);

// https://via.placeholder.com/150

Card.propTypes = {
  store: PropTypes.shape({
    storeId: PropTypes.string,
    status: PropTypes.string,
    tradingName: PropTypes.string,
    _id: PropTypes.string,
  }),
  onPressStoreCard: PropTypes.func.isRequired,
  isSearch: PropTypes.bool.isRequired,
};

Card.defaultProps = {
  store: {},
};

export default Card;
