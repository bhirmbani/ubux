import { createSelector } from 'reselect';

/**
 * Subscribe to main reducer
 *
 * see './src/reducers.js'
 * @return Object
 */

export const selectStoreDetailReducer = state => state.storeDetail;

export const selectStoreDetail = () => createSelector(selectStoreDetailReducer, state => state.storeDetail);

export const selectStoreProduct = () => createSelector(selectStoreDetailReducer, state => state.storeProduct);

export const selectProductDetail = () => createSelector(selectStoreDetailReducer, state => state.productDetail);
