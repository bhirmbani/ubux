import { StyleSheet } from 'react-native';
import helpers from '../../helpers';

const ShoppingCart = StyleSheet.create({
  emptyParent: {
    margin: 10,
  },
  emptyText: {
    textAlign: 'center',
  },
});

const ShoppingCartList = StyleSheet.create({
  listHeaderParent: {
    margin: 10,
  },
  listHeaderText: {
    textAlign: 'center',
  },
  childrenImage: {
    width: helpers.styles.WIDTH_HALF - 20,
    height: helpers.styles.FULL_HEIGHT / 5 - 35,
    borderRadius: 10,
  },
});

const ShoppingCartItem = StyleSheet.create({
  parentShape: {
    margin: 10,
  },
  childrenShape: {
    textAlign: 'center',
    width: helpers.styles.WIDTH_HALF - 20,
    // height: helpers.styles.FULL_HEIGHT / 5 + 15,
    alignItems: 'center',
    // borderWidth: 1,
    borderRadius: 10,
    // borderColor: helpers.styles.constantStyles.borderColor,
  },
  childrenImage: {
    width: helpers.styles.WIDTH_HALF - 20,
    height: helpers.styles.FULL_HEIGHT / 4 - 35,
    borderRadius: 10,
  },
  listHeaderParent: {
    margin: 10,
  },
  listHeaderText: {
    textAlign: 'center',
  },
  textParent: {
    justifyContent: 'space-between',
    width: helpers.styles.WIDTH_HALF,
    display: 'flex',
    flexDirection: 'row',
    marginVertical: 10,
    // backgroundColor: 'yellow',
  },
  textChild: {
    justifyContent: 'center',
    width: helpers.styles.WIDTH_HALF - 200,
    marginHorizontal: 10,
    // backgroundColor: 'red',
  },
  text: { textAlign: 'center' },
});

export default {
  ShoppingCart,
  ShoppingCartList,
  ShoppingCartItem,
};
