import { put, call, takeLatest } from 'redux-saga/effects';
// import Reactotron from 'reactotron-react-native';
import Api from './services';
import types from './constants';
import * as actions from './reducer';
// import * as selectors from './selectors'

/**
 * This method will get all stores from database.
 */
function* watchGetStoreDetail({ payload }) {
  try {
    const response = yield call(Api.getStoreDetail, payload);
    const { data } = response;
    const resultPayload = {
      content: data.store,
      message: data.message,
    };
    if (data.success) {
      return yield put(actions.getStoreDetailResult(resultPayload));
    }
    return yield put(actions.getStoreDetailResult({ ...resultPayload, content: {} }));
  } catch (error) {
    return yield put(actions.getStoreDetailResult({ content: {}, message: error.message }));
  }
}

/**
 * This method will get all store's product
 */
function* watchGetStoreProduct({ payload }) {
  try {
    const response = yield call(Api.getStoreProduct, payload);
    const { data } = response;
    const resultPayload = {
      content: data.products,
      message: data.message,
    };
    if (data.success) {
      return yield put(actions.getStoreProductResult(resultPayload));
    }
    return yield put(actions.getStoreProductResult({ ...resultPayload, content: {} }));
  } catch (error) {
    return yield put(actions.getStoreProductResult({ content: {}, message: error.message }));
  }
}

/**
 * This will get product detail
 */
function* watchGetProductDetail({ payload }) {
  try {
    const response = yield call(Api.getProductDetail, payload);
    const { data } = response;
    const resultPayload = {
      content: data.product,
      message: data.message,
    };
    if (data.success) {
      return yield put(actions.getProductDetailResult(resultPayload));
    }
    return yield put(actions.getProductDetailResult({ ...resultPayload, content: {} }));
  } catch (error) {
    return yield put(actions.getProductDetailResult({ content: {}, message: error.message }));
  }
}

const storeDetailSagas = [
  takeLatest(types.GET_STORE_DETAIL, watchGetStoreDetail),
  takeLatest(types.GET_STORE_PRODUCT, watchGetStoreProduct),
  takeLatest(types.GET_PRODUCT_DETAIL, watchGetProductDetail),
];

export default storeDetailSagas;
