import React from 'react';
import { View } from 'react-native';
import { H2 } from 'native-base';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { bindActionCreators } from 'redux';

import styles from './styles';

import * as actions from './reducer';
import * as selectors from './selectors';

import List from './List';

class ShoppingCart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    // const { props } = this;
  }

  /**
   * This method will trigger action that remove a product
   * from shopping cart
   * @param {number} idx - index of the product
   */
  onPressRemove = (idx) => {
    const { props } = this;
    const payload = {
      idx,
    };
    props.removeFromCart(payload);
  }

  render() {
    return (
      <View>
        {this.props.shoppingCart.content.length < 1 ? (
          <View style={styles.ShoppingCart.emptyParent}>
            <H2 style={styles.ShoppingCart.emptyText}>Your shopping cart is empty</H2>
          </View>
        ) : (<List shoppingCart={this.props.shoppingCart} onPressRemove={this.onPressRemove} />)
      }
      </View>
    );
  }
}

ShoppingCart.propTypes = {
  shoppingCart: PropTypes.shape({
    content: PropTypes.array,
  }).isRequired,
};

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    removeFromCart: actions.removeFromCart,
  },
  dispatch,
);

const mapStateToProps = () => createStructuredSelector({
  shoppingCart: selectors.selectShoppingCart(),
});

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingCart);
