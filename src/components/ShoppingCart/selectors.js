import { createSelector } from 'reselect';

/**
 * Subscribe to reducers
 *
 * see './src/reducers.js'
 * @return Object
 */

export const selectStoreDetailReducer = state => state.storeDetail;

export const selectShoppingCartReducer = state => state.shoppingCart;

export const selectStoreDetail = () => createSelector(selectStoreDetailReducer, state => state.storeDetail);

export const selectShoppingCart = () => createSelector(selectShoppingCartReducer, state => state.shoppingCart);

export const selectStoreProduct = () => createSelector(selectStoreDetailReducer, state => state.storeProduct);

export const selectProductDetail = () => createSelector(selectStoreDetailReducer, state => state.productDetail);
