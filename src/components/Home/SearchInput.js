import React from 'react';
import { View } from 'react-native';
import {
  Input, Item, Button, Text,
} from 'native-base';
import PropTypes from 'prop-types';

import styles from './styles';

const SearchInput = props => (
  <View style={styles.SearchInput.parentShape}>
    <Item>
      <Input
        value={props.state.search}
        onChangeText={search => props.onChangeSearchInput(search)}
        onSubmitEditing={() => props.onPressSubmitSearch()}
        placeholder="search store"
      />
      {props.isSearch
        && (
          <View style={{ justifyContent: 'center' }}>
            <Button onPress={() => props.onPressReset()} small rounded success>
              <Text>Reset</Text>
            </Button>
          </View>
        )}
    </Item>
  </View>
);

SearchInput.propTypes = {
  state: PropTypes.shape({
    search: PropTypes.string.isRequired,
  }).isRequired,
  onChangeSearchInput: PropTypes.func.isRequired,
  onPressSubmitSearch: PropTypes.func.isRequired,
  isSearch: PropTypes.bool.isRequired,
  onPressReset: PropTypes.func.isRequired,
};

export default SearchInput;
