import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { H3, Text, Button } from 'native-base';
import PropTypes from 'prop-types';
import FastImage from 'react-native-fast-image';

import styles from './styles';
import globalHelpers from '../../helpers';

const ProductDetail = ({ navigation }) => (
  <View
    style={styles.ProductDetail.parentShape}
  >
    <View style={styles.ProductDetail.childrenShape}>
      <FastImage
        style={styles.ProductDetail.childrenImage}
        source={{
          uri: `https://via.placeholder.com/${globalHelpers.styles.FULL_WIDTH}`,
          priority: FastImage.priority.low,
        }}
        resizeMode={FastImage.resizeMode.cover}
      />
      <View style={styles.ProductDetail.textParent}>
        <View style={styles.ProductDetail.textChild}>
          <H3 style={{ textAlign: 'center' }}>{navigation.state.params.productDetail.content.name}</H3>
        </View>
        <View style={styles.ProductDetail.textChild}>
          <Text style={{ textAlign: 'center' }}>{navigation.state.params.productDetail.content.priceCash}</Text>
        </View>
      </View>
      <Button success onPress={() => navigation.state.params.pushToShoppingCart(navigation.state.params.productDetail.content)} full>
        <Text>Buy</Text>
      </Button>
    </View>
  </View>
);

ProductDetail.propTypes = {
  navigation: PropTypes.instanceOf(Object).isRequired,
};

export default ProductDetail;
