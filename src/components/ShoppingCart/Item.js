import React from 'react';
import { View } from 'react-native';
import { Text, Button } from 'native-base';
import PropTypes from 'prop-types';
import FastImage from 'react-native-fast-image';

import styles from './styles';
import globalHelpers from '../../helpers';

const ShoppingCartItem = props => (
  <View
    style={styles.ShoppingCartItem.parentShape}
  >
    <View style={styles.ShoppingCartItem.childrenShape}>
      <FastImage
        style={styles.ShoppingCartItem.childrenImage}
        source={{
          uri: `https://via.placeholder.com/${globalHelpers.styles.FULL_WIDTH}`,
          priority: FastImage.priority.low,
        }}
        resizeMode={FastImage.resizeMode.cover}
      />
      <View style={styles.ShoppingCartItem.textParent}>
        <View style={styles.ShoppingCartItem.textChild}>
          <Text style={styles.ShoppingCartItem.text}>{props.shoppingCart.name}</Text>
        </View>
        <View style={styles.ShoppingCartItem.textChild}>
          <Text style={styles.ShoppingCartItem.text}>{props.shoppingCart.priceCash}</Text>
        </View>
      </View>
      <Button onPress={() => props.onPressRemove(props.idx)} danger full>
        <Text>Remove</Text>
      </Button>
    </View>
  </View>
);

ShoppingCartItem.propTypes = {
  shoppingCart: PropTypes.instanceOf(Object).isRequired,
  idx: PropTypes.number.isRequired,
  onPressRemove: PropTypes.func.isRequired,
};

export default ShoppingCartItem;
