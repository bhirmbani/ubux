import { StyleSheet } from 'react-native';
import helpers from '../../helpers';

const Card = StyleSheet.create({
  parentShape: {
    margin: 10,
    // flex: 1,
    // height: '100%',
  },
  childrenShape: {
    textAlign: 'center',
    width: helpers.styles.WIDTH_HALF - 20,
    height: helpers.styles.FULL_HEIGHT / 5 + 15,
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 10,
    borderColor: helpers.styles.constantStyles.borderColor,
  },
  childrenImage: {
    width: helpers.styles.WIDTH_HALF - 20,
    height: helpers.styles.FULL_HEIGHT / 5 - 35,
    borderRadius: 10,
  },
  textParent: {
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 5,
  },
});

const FilterChecklist = StyleSheet.create({
  parentShape: {
    flexDirection: 'row', justifyContent: 'space-around',
  },
  childrenShape: { width: 100, borderBottomWidth: 0 },
  textShape: { marginLeft: 5 },
});

const SearchInput = StyleSheet.create({
  parentShape: { marginHorizontal: 10 },
});

export default {
  Card,
  FilterChecklist,
  SearchInput,
};
